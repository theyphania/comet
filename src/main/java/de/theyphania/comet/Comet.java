package de.theyphania.comet;

import de.theyphania.comet.commands.BaseCommand;
import de.theyphania.comet.utils.LangManager;
import dev.jorel.commandapi.CommandAPI;

public class Comet extends CometPlugin
{
	private static Comet instance;
	public Comet()
	{
		instance = this;
	}
	public static Comet getInstance()
	{
		return instance;
	}
	@Override
	public void onEnableInner()
	{
		log("Loading config.yml...");
		saveDefaultConfig();
		log("Success!");
		log("Loading language files...");
		new LangManager();
		log("Success!");
		log("Loading commands...");
		CommandAPI.registerCommand(BaseCommand.class);
		log("Success!");
	}
}
