package de.theyphania.comet.commands;

import de.theyphania.comet.Comet;
import de.theyphania.comet.utils.LangManager;
import de.theyphania.comet.utils.Language;
import de.theyphania.comet.utils.Message;
import dev.jorel.commandapi.annotations.Alias;
import dev.jorel.commandapi.annotations.Command;
import dev.jorel.commandapi.annotations.Default;
import dev.jorel.commandapi.annotations.Permission;
import dev.jorel.commandapi.annotations.Subcommand;
import org.bukkit.command.CommandSender;

import java.util.Map;

@Command("comet")
@Alias({"comet:comet"})
@Permission("comet.command.base")
public class BaseCommand
{
	private static final Comet plugin = Comet.getInstance();
	private static final LangManager lm = LangManager.getInstance();
	private static final Map<String, String> commandMap = Map.of(
		"command.base", "<click:run_command:/comet>/comet</click>",
		"command.base.help", "<click:run_command:/comet help>/comet help</click>",
		"command.base.lang", "<click:run_command:/comet lang>/comet lang</click>",
		"command.base.reload", "<click:run_command:/comet reload>/comet reload</click>"
	);
	
	@Default
	public static void ve(CommandSender sender)
	{
		sender.sendMessage(lm.getComponent(Message.COMMAND_BASE_TEXT, sender, commandMap));
	}
	
	@Subcommand("help")
	public static void veHelp(CommandSender sender)
	{
		sender.sendMessage(lm.getComponent(Message.COMMAND_BASE_HELP_TEXT, sender, commandMap));
	}
	@Subcommand("lang")
	public static void veLang(CommandSender sender)
	{
		StringBuilder stringBuilder = new StringBuilder();
		for (Language lang : Language.values())
		{
			stringBuilder.append("\n- <hover:show_text:'").append(lang.toString()).append("'>").append(lang.getLanguage()).append("</hover>");
		}
		Map<String, String> languageMap = Map.of("languages", stringBuilder.toString());
		sender.sendMessage(lm.getComponent(Message.COMMAND_BASE_LANG_TEXT, sender, languageMap));
	}
	@Subcommand("reload")
	public static void veReload(CommandSender sender)
	{
		plugin.reloadConfig();
		lm.reload();
		sender.sendMessage(lm.getComponent(Message.COMMAND_BASE_RELOAD_SUCCESS, sender));
	}
}
