package de.theyphania.comet.utils;

public enum Message
{
	COMMAND_BASE_TEXT,
	COMMAND_BASE_HELP_TEXT,
	COMMAND_BASE_LANG_TEXT,
	COMMAND_BASE_RELOAD_SUCCESS,
	MINIMESSAGE_TEST;
	
	
	@Override
	public String toString()
	{
		return super.toString().replace('_', '.').toLowerCase();
	}
}
