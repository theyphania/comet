import org.apache.tools.ant.filters.ReplaceTokens

plugins {
    `java-library`
    idea
    id("com.github.johnrengelman.shadow") version "6.0.0"
}


group = project.property("pluginGroup") as String
version = project.property("pluginVersion") as String

repositories {
    mavenCentral()
    maven {
        url = uri("https://papermc.io/repo/repository/maven-public/")
    }
    maven {
        url = uri("https://jitpack.io")
    }
    maven {
        url = uri("https://raw.githubusercontent.com/JorelAli/CommandAPI/mvn-repo/")
    }
    maven {
        url = uri("https://repo.codemc.org/repository/maven-public/")
    }
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
    }
}

dependencies {
    implementation("io.papermc.paper:paper-api:" + project.property("versionPaper") as String)
    implementation("dev.jorel.CommandAPI:commandapi-core:" + project.property("versionCommandApi") as String)
    implementation("dev.jorel.CommandAPI:commandapi-annotations:" + project.property("versionCommandApi") as String)
    implementation("org.jetbrains:annotations:19.0.0")
    annotationProcessor("dev.jorel.CommandAPI:commandapi-annotations:" + project.property("versionCommandApi") as String)
    implementation("net.kyori:adventure-text-minimessage:" + project.property("versionMiniMessage") as String)
}

sourceSets {
    main {
        java.srcDir("src/main/java")
        resources.srcDir("src/main/resources")
    }
}

java {
    sourceCompatibility = JavaVersion.valueOf(project.property("versionJava") as String)
    targetCompatibility = JavaVersion.valueOf(project.property("versionJava") as String)
}

tasks {
    compileJava {
        options.encoding = project.property("projectEncoding") as String
    }
    shadowJar {
        dependencies {
            include(dependency("net.kyori:adventure-text-minimessage:4.1.0-SNAPSHOT"))
        }
    }
    processResources {
        outputs.upToDateWhen { false }
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        val tokens = mapOf(
                "group" to project.property("pluginGroup") as String,
                "name" to project.property("pluginName") as String,
                "prefix" to project.property("pluginPrefix") as String,
                "ver" to project.property("pluginVersion") as String,
                "apiVer" to project.property("pluginApiVersion") as String,
                "author" to project.property("pluginAuthor") as String,
                "description" to project.property("pluginDescription") as String,
                "homepage" to project.property("pluginHomepage") as String
        )
        inputs.properties(tokens)
        from(sourceSets.main.get().resources.srcDirs) {
            filter<ReplaceTokens>("tokens" to tokens)
        }
    }
}

